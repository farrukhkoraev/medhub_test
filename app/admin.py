from django.contrib import admin

# Register your models here.
from app.models import MedicalRecord, MedicalHistory, FormField, MedicalForm

admin.site.register(MedicalRecord)
admin.site.register(MedicalHistory)
admin.site.register(MedicalForm)
admin.site.register(FormField)
