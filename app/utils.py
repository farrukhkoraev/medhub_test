form_fields = {
    1: [
        ('has_headache', 'bool'),
        ('drinks_or_smokes', 'bool'),
        ('does_sport', 'bool'),
        ('temperature', 'float')
    ],
    2: [
        ('knee_pain', 'bool'),
        ('last_activity', 'date'),
        ('pressure', 'float')
    ]
}
