from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class MedicalHistory(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    age = models.IntegerField(default=18)
    created_at = models.DateTimeField(auto_now_add=True)


class MedicalRecord(models.Model):
    physician = models.ForeignKey(to=User, on_delete=models.SET_NULL, related_name='records', null=True)
    history = models.ForeignKey(to=MedicalHistory, on_delete=models.CASCADE, related_name='records')
    form_type = models.IntegerField()
    form_details = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)


class MedicalForm(models.Model):
    type = models.IntegerField(unique=True)


class FormField(models.Model):
    form = models.ForeignKey(to=MedicalForm, on_delete=models.CASCADE, related_name='fields')
    name = models.CharField(max_length=64)
    type = models.CharField(max_length=64)
