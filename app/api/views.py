from rest_framework import status, viewsets, mixins, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from . import serializers
from . import services as svc
from .. import models


class MedicalHistoryViewSet(mixins.ListModelMixin,
                            mixins.RetrieveModelMixin,
                            mixins.CreateModelMixin,
                            viewsets.GenericViewSet):
    queryset = models.MedicalHistory.objects.all()
    serializer_class = serializers.MedicalHistorySerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        if self.action == 'records':
            return serializers.MedicalRecordSerializer
        return super().get_serializer_class()

    @action(methods=['get'], detail=True)
    def records(self, request, *args, **kwargs):
        history = self.get_object()
        serializer = self.get_serializer(history.records, many=True)
        return Response(serializer.data)


class MedicalRecordViewSet(mixins.RetrieveModelMixin,
                           mixins.CreateModelMixin,
                           viewsets.GenericViewSet):
    queryset = models.MedicalRecord.objects.all()
    serializer_class = serializers.MedicalRecordSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        record = svc.create_record(user=request.user, data=serializer.validated_data)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class MedicalFormView(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.MedicalFormSerializer
    queryset = models.MedicalForm.objects.all()
    lookup_field = 'type'
    permission_classes = [permissions.IsAuthenticated]
