import datetime

from django.core.exceptions import ValidationError
from rest_framework import serializers
from . import services as svc
from app.models import MedicalRecord, MedicalHistory, FormField, MedicalForm


class MedicalRecord1Serializer(serializers.Serializer):
    has_headache = serializers.BooleanField()
    drinks_or_smokes = serializers.BooleanField()
    does_sport = serializers.BooleanField()
    temperature = serializers.FloatField(min_value=32, max_value=40)


class MedicalRecord2Serializer(serializers.Serializer):
    knee_pain = serializers.BooleanField()
    last_activity = serializers.DateField()
    pressure = serializers.FloatField(min_value=100, max_value=200)

    def validate_last_activity(self, value):
        if value > datetime.date.today():
            raise ValidationError(f'Value ({value}) can not be in future')


class MedicalRecordSerializer(serializers.ModelSerializer):
    form_type = serializers.IntegerField(max_value=2, min_value=1)

    class Meta:
        model = MedicalRecord
        fields = ['physician', 'history', 'form_type', 'created_at', 'form_details']
        read_only_fields = ['physician', 'created_at']

    def validate_type(self, form_type: int, age: int):
        if age > 30 and form_type == 1:
            raise ValidationError(
                {'type': f'Invalid value for age{age}. Must be 2'}
            )
        if 17 < age <= 30 and form_type == 2:
            raise ValidationError(
                {'type': f'Invalid value for age {age}. Must be 1'}
            )

    def validate_form_details(self, form_type: int, data: dict):
        if form_type == 1:
            serializer = MedicalRecord1Serializer(data=data)
        else:
            serializer = MedicalRecord2Serializer(data=data)
        serializer.is_valid(raise_exception=True)

    def validate_patients_age(self, value):
        if value < 18:
            raise ValidationError({'age': 'Cannot create record for patients under 18'})

    def validate(self, attrs):
        form_type = attrs.get('form_type')
        self.validate_patients_age(attrs['history'].age)
        self.validate_form_details(form_type, attrs['details'])
        self.validate_type(form_type, attrs['history'].age)
        return attrs


class MedicalHistorySerializer(serializers.ModelSerializer):
    avg_temperature = serializers.SerializerMethodField()
    avg_pressure = serializers.SerializerMethodField()

    def get_avg_temperature(self, obj):
        return svc.average_temperature(obj)

    def get_avg_pressure(self, obj):
        return svc.average_pressure(obj)

    class Meta:
        model = MedicalHistory
        fields = '__all__'


class FormFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormField
        fields = ['name', 'type']


class MedicalFormSerializer(serializers.ModelSerializer):
    fields = FormFieldSerializer(many=True)

    class Meta:
        model = MedicalForm
        fields = '__all__'
