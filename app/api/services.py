import dataclasses
from datetime import datetime

from django.db.models import Avg, F
from rest_framework.exceptions import ValidationError

from django.contrib.auth import get_user_model
from rest_framework.generics import get_object_or_404
from ..utils import form_fields
from app.models import MedicalRecord, MedicalHistory

User = get_user_model()


def create_record(user: User, data: dict):
    history = data.get('history')
    if history.age < 18:
        raise ValidationError(
            {'age': "Must be greater than 18"}
        )

    model = MedicalRecord(physician=user, **data)
    model.save()
    return model


def list_records(history_id: int):
    return MedicalRecord.objects.filter(pk=history_id)


def retrieve_record(id_: int):
    return get_object_or_404(MedicalRecord.objects.all(), pk=id_)


def list_form_detail_fields(type_: int) -> list:
    fields = form_fields.get(type_, [])
    result = [{'name': f, 'type': t} for f, t in fields]
    return result


def average_temperature(history: MedicalHistory) -> float:
    records = history.records.filter(details__has_key='temperature').all()
    temperature = 0
    if records:
        temperature = sum(r.details['temperature'] for r in records) / len(records)

    return temperature


def average_pressure(history: MedicalHistory) -> float:
    records = history.records \
        .filter(details__has_key='pressure').all()
    pressure = 0
    if records:
        pressure = sum(r.details['pressure'] for r in records) / len(records)

    return pressure
