from django.urls import path, include
from rest_framework.routers import SimpleRouter

from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = SimpleRouter()
router.register(r'history', views.MedicalHistoryViewSet)
router.register(r'record', views.MedicalRecordViewSet)
router.register(r'form', views.MedicalFormView)

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(router.urls)),
]
