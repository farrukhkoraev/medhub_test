# Medhub Test Task


## Getting started
To run create .env file and populate following fields: DB_NAME, DB_USER, DB_PASSWORD, DB_HOST.
Swagger documentation can be found at http://host:port/swagger/ endpoint. 
Can be run both on local machine and in docker container. 
