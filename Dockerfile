FROM python:3.9-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1


RUN apk update && apk add gcc libc-dev

RUN mkdir /code

COPY requirements.txt /code
WORKDIR /code
RUN pip install -r requirements.txt
COPY . /code
